function getCurrentDate(){
    let dateNow = new Date();
    let Out = document.getElementById("outputCurrentDate");
    Out.innerHTML = "Дата: " +dateNow.getDate() + "<br/>День: " + dateNow.getDay() + "<br/>Час: " + dateNow.getHours();
}

function getDayOfWeek(){
    let dateStr = document.getElementById("dateInputToDay").value;
    let date = new Date(parseInt(dateStr.slice(0, 4)), parseInt(dateStr.slice(5, 7)), parseInt(dateStr.slice(8)));
    let Out = document.getElementById("outputDayAndDate");
    let days = ["Неділя", "Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця", "Субота"]
    Out.innerHTML = "Дата: " + date.getDate() + "<br/>День: " + days[date.getDay() - 3];
    let day = {
        dayNumber : date.getDate(),
        dayName : days[date.getDay()]
    }
}

function NDaysAgo(){
    let N = parseInt(document.getElementById("inputNDays").value);
    let date = new Date();
    date.setDate(date.getDate() - N);
    let Out = document.getElementById("outputDate");
    Out.innerHTML = "Рік: " + date.getFullYear()  + "<br/>Місяць: "  +date.getMonth() + "<br/>Число: " + date.getDate();
}

function GetLastDay(){
    let year = document.getElementById("inputYearMonth").value.slice(0, 4);
    let month = document.getElementById("inputYearMonth").value.slice(5);
    let date = new Date(parseInt(year), parseInt(month), 0);
    document.getElementById("outputLastDay").innerText = date.getDate() + " ";
}

function GetSeconds(){
    let date = new Date()
    let dateStart = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0);
    let startSpan = Math.round(((date - dateStart)/1000));
    let endSpan = Math.round(((24*60*60 - startSpan)));
    document.getElementById("outputSpans").innerHTML ="Від початку дня: " + startSpan + "<br/>До кінця дня: " + endSpan;
    let spans = {
        secondsFromStart : startSpan,
        secondsToEnd : endSpan
    }
}

function GetStuff(){
    let dateStr = document.getElementById("inputDate").value;
    let yearThousand = parseInt(dateStr.slice(-dateStr.length, -3)) + 1;
    let century = parseInt(dateStr.slice(-dateStr.length, -2)) + 1;
    let half;
    if(parseInt(dateStr.slice(-2)) < 50)
    {
        half = 1;
    }
    else half = 2;
    document.getElementById("outputNums").innerHTML = "Тисячоліття: " +yearThousand + "<br>Століття: " + century + "<br> Половина: " + half;
}

function GetFullYears(){
    let dateArr1 = document.getElementById("inputDatePast1").value.split(".");
    let dateArr2 = document.getElementById("inputDatePast2").value.split(".");
    let date1 = new Date(parseInt(dateArr1[2]), parseInt(dateArr1[1]), parseInt(dateArr1[0]), 0, 0, 0, 0);
    let date2 = new Date(parseInt(dateArr2[2]), parseInt(dateArr2[1]), parseInt(dateArr2[0]), 0, 0, 0, 0);
    let span = (date2 - date1)/1000;
    let years = Math.floor(span/(365*24*60*60));
    document.getElementById("outputFullYears").innerText = years + "";
}

function Clock(){
    let timeStr1 = document.getElementById("inputClock1").value;
    let timeStr2 = document.getElementById("inputClock2").value;
    let date1 = new Date(0, 0, 0, parseInt(timeStr1.slice(-timeStr1.length, -3)), parseInt(timeStr1.slice(-2)), 0, 0);
    let date2 = new Date(0, 0, 0, parseInt(timeStr2.slice(-timeStr2.length, -3)), parseInt(timeStr2.slice(-2)), 0, 0);
    console.log(date1.getHours() + " " + date1.getMinutes());
    console.log(date2.getHours() + " " + date2.getMinutes());
    let difference = ((date1.getHours() * 60 + date1.getMinutes()) + (date2.getHours() * 60 + date2.getMinutes()))/2 - 720;
    document.getElementById("outputClock").innerText = difference + "";
}